﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models.Users
{
    public class CreateUserModel
    {
        public CreateUserModel()
        {
            Roles = new string[0];
        }

        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string[] Roles { get; set; }
    }
}