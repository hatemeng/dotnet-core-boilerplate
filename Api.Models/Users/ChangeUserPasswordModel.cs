﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models.Users
{
    public class ChangeUserPasswordModel
    {
        [Required]
        public string Password { get; set; }
    }
}