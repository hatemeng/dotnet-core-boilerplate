﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models.Users
{
    public class UpdateUserModel
    {
        public UpdateUserModel()
        {
            Roles = new string[0];
        }

        [Required]
        public string Username { get; set; }
        public string[] Roles { get; set; }
    }
}