﻿using System.Collections.Generic;

namespace Api.Models.Users
{
    public class UserModel
    {
        public UserModel()
        {
            Roles = new string[0];
        }

        public int Id { get; set; }
        public string Username { get; set; }

        public string[] Roles { get; set; }
    }
}