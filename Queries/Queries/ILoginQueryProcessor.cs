﻿using System.Threading.Tasks;
using Api.Models.Login;
using Api.Models.Users;
using Data.Model;
using Queries.Models;

namespace Queries.Queries
{
    public interface ILoginQueryProcessor
    {
        UserWithToken Authenticate(string username, string password);
        Task<User> Register(RegisterModel model);
        Task ChangePassword(ChangeUserPasswordModel requestModel);
    }
}