﻿using System.Linq;
using AutoMapper;
using Api.Models.Users;
using Data.Model;
using Queries.Models;

namespace Api.Maps
{
    public class UserWithTokenMap : IAutoMapperTypeConfigurator
    {
        public void Configure(IMapperConfigurationExpression configuration)
        {
            var map = configuration.CreateMap<UserWithToken, UserWithTokenModel>();
        }
    }
}