﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Api.Helpers
{
    public interface IActionTransactionHelper
    {
        void BeginTransaction();
        void EndTransaction(ActionExecutedContext filterContext);
        void CloseSession();
    }
}