# .Net Core Boilerplate

A template for DotNet Core projects.

## Project structure
- **Api**: Project for controllers, mapping between domain model and API model, API configuration
- **Api.Common**: At this point, there are collected exception classes that are interpreted in a certain way by filters to return correct HTTP codes with errors to the user
- **Api.Models**: Project for API models
- **Data.Access**: Project for interfaces and implementation of the Unit of Work pattern
- **Data.Model**: Project for domain model
- **Queries**: Project for query processors and query-specific classes
- **Security**: Project for the interface and implementation of the current user's security context

## Source
- Toptal tutorial: https://www.toptal.com/asp-dot-net/asp-net-web-api-tutorial
- Github: https://github.com/dimangulov/expenses

## ToDo
1. Convert used database to Postgres instead of the current MSSQL
2. Add integrations project (ex. FreeIPA)
3. Use hangfile to run background services
4. Add services project to contain background services logic
