﻿using Data.Access.Maps.Common;
using Data.Model;
using Microsoft.EntityFrameworkCore;

namespace Data.Access.Maps.Main
{
    public class RoleMap : IMap
    {
        public void Visit(ModelBuilder builder)
        {
            builder.Entity<Role>()
                .ToTable("Roles")
                .HasKey(x => x.Id);
        }
    }
}