﻿using Data.Access.Maps.Common;
using Data.Model;
using Microsoft.EntityFrameworkCore;

namespace Data.Access.Maps.Main
{
    public class UserRoleMap : IMap
    {
        public void Visit(ModelBuilder builder)
        {
            builder.Entity<UserRole>()
                .ToTable("UserRoles")
                .HasKey(x => x.Id);
        }
    }
}