﻿using Data.Access.Maps.Common;
using Data.Model;
using Microsoft.EntityFrameworkCore;

namespace Data.Access.Maps.Main
{
    public class UserMap : IMap
    {
        public void Visit(ModelBuilder builder)
        {
            builder.Entity<User>()
                .ToTable("Users")
                .HasKey(x => x.Id);
        }
    }
}