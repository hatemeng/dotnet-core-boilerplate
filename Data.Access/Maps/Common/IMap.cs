﻿using Microsoft.EntityFrameworkCore;

namespace Data.Access.Maps.Common
{
    public interface IMap
    {
        void Visit(ModelBuilder builder);
    }
}
